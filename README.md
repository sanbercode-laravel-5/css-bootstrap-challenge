# Challenge CSS Bootstrap

## Target
* Membuat layout HTML dengan CSS Bootstrap

## Petunjuk Pengerjaan
1. Menambahkan Bootstrap pada HTML
Buatlah sebuah file html dengan nama index.html lalu masukkan blok kode di bawah ini di bagian head. 

```html
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
```
2. Membaca wireframe/layout web
Kita akan membuat mini e-commerce. Berikut ini adalah layout dari website yang ingin kita buat: 
![Layout Website](wireframe.png)

Beberapa ketentuan: 
- untuk Bagian Jumbotron boleh menggunakan gambar statis saja atau slider atau carousel (silakan cari di google)
- Box ITEM1 dst adalah Card yang memuat gambar (silakan cari component Card di Bootstrap). Di dalam Card tersebut harus memuat informasi harga, deskripsi, dan button Tambahkan ke Keranjang. 

3. Mengimplementasikan layout dengan Bootstrap
Setelah melihat layoutnya, coba implementasikan Bootstrap ke dalam index.html yang sudah kita buat agar tampilannya sesuai. Sangat dibolehkan untuk berkreasi menggunakan component atau class yang tersedia di Bootstrap. 


